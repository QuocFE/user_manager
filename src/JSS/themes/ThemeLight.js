export const light={
    background:'#eeecec',
    color:'#181a1b',
    borderColor:'#757575',
    focusColorTextField:'#5264AE',
}
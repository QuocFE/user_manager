
export const darkNight={
    background:'#181a1b',
    color:'#cac5be',
    borderColor:'#757575',
    focusColorTextField:'#e88802',
}
import styled from 'styled-components'


export const Select = styled.select`
background-color:transparent;
color:${props => props.theme.color};
padding:12px;
width:50%;
font-weight:bold;
border:1px solid transparent;
border-bottom-color:${props => props.theme.borderColor};
outline:none;
transition:all .2s;
&{
    option{
        background-color:${props => props.theme.background};
        color:${props => props.theme.color};
    }
}
`
import styled from 'styled-components'


export const H1=styled.h1`
color:${props=>props.theme.color};
text-align:center;
padding:20px 0
`
export const H3=styled.h3`
color:${props=>props.theme.color};
padding:20px 0
`
export const H4=styled.h4`
color:${props=>props.theme.color};
padding:15px 0
`
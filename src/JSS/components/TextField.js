import styled from 'styled-components'


export const TextField = styled.div`
&.group{ 
    position:relative; 
    margin-bottom:45px;
    input{
        font-size:18px;
        color:${props => props.theme.color};
        background-color:transparent;
        padding:10px 10px 10px 5px;
        display:block;
        width:100%;
        border:none;
        border-bottom:1px solid ${props => props.theme.borderColor};
      }
    .icon-check{
      position:absolute;
      top:30%;
      right:0; 
    }
      input:focus 		{ outline:none; }
      
      /* LABEL ======================================= */
      label 				 {
        color:${props => props.theme.color}; 
        font-size:18px;
        font-weight:normal;
        position:absolute;
        pointer-events:none;
        left:5px;
        top:10px;
        transition:0.2s ease all; 
      }
      
      /* active state */
      input:focus ~ label, input:valid ~ label 		{
        top:-20px;
        font-size:14px;
        color:${props => props.theme.focusColorTextField};
      }
      
      /* BOTTOM BARS ================================= */
      .bar 	{ position:relative; display:block; width:100%; }
      .bar:before, .bar:after 	{
        content:'';
        height:2px; 
        width:0;
        bottom:1px; 
        position:absolute;
        background:${props => props.theme.focusColorTextField}; 
        transition:0.2s ease all; 
      }
      .bar:before {
        left:50%;
      }
      .bar:after {
        right:50%; 
      }
      
      /* active state */
      input:focus ~ .bar:before, input:focus ~ .bar:after {
        width:50%;
      }
      
      /* HIGHLIGHTER ================================== */
      .highlight {
        position:absolute;
        height:60%; 
        width:100px; 
        top:25%; 
        left:0;
        pointer-events:none;
        opacity:0.5;
      }
      
      /* active state */
      input:focus ~ .highlight {
        animation:inputHighlighter 0.3s ease;
      } 
  }

  
  /* ANIMATIONS ================ */
  @keyframes inputHighlighter {
      from { background:#5264AE; }
    to 	{ width:0; background:transparent; }
  }
`
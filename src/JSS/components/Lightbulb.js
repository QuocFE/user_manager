import styled from 'styled-components'

export const Lightbulb = styled.div`
position:absolute;
top:10%;
right:10%;
&{
    label{
        position:relative;
        cursor:pointer;
        i{
            position:absolute;
            left:-100%;
            width:50px;
            height:50px;
            background-color:#181a1b;
            text-align:center;
            line-height:50px;
            font-size:30px;
            border-radius:50%;
            transition:all .3s;
        }
    }
}
`
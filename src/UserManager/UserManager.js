import React, { Component } from 'react'
import { ThemeProvider } from 'styled-components'
import { H1 } from '../JSS/components/Heading'
import { Lightbulb } from '../JSS/components/Lightbulb'
import { ContainerFluid } from '../JSS/Container/Container'
import FormRegister from './FormRegister'
import ListUser from './ListUser'
import { connect } from 'react-redux'
import { actChangeTheme } from '../redux/actions/userManagerAction'

class UserManager extends Component {
    state = {
        lightBulb: "yellow",
    }
    render() {
        let { theme, changeTheme } = this.props;
        return (
            <div>
                <ThemeProvider theme={theme}>
                    <ContainerFluid>
                        <Lightbulb>
                            <label for='switch'>
                                <i style={{ color: this.state.lightBulb }} class="fa fa-moon" onClick={() => {
                                    let { lightBulb } = this.state;
                                    let color = lightBulb === "yellow" ? "white" : "yellow";
                                    this.setState({ lightBulb: color })
                                }}>
                                </i>
                                <input id='switch' type='checkbox' onChange={changeTheme} />
                            </label>
                        </Lightbulb>
                        <H1>User Manager</H1>
                        <FormRegister></FormRegister>
                        <ListUser></ListUser>
                    </ContainerFluid>
                </ThemeProvider>
            </div>
        )
    }
}
const mapStateToProp = (stateRoot) => {
    return {
        theme: stateRoot.userManagerReducer.theme
    }
}
const mapDispatchToProp = (dispatch) => {
    return {
        changeTheme: (event) => {
            dispatch(actChangeTheme(event))
        }
    }
}
export default connect(mapStateToProp, mapDispatchToProp)(UserManager)
import React, { Component } from 'react'
import { H3 } from '../JSS/components/Heading';
import { TextField } from '../JSS/components/TextField';
import { Select } from '../JSS/components/Select';
import { Hr } from '../JSS/components/HightLight';
import { connect } from 'react-redux';
import { actChangeDisabled, actDegister, actUpdateAccount } from '../redux/actions/userManagerAction';
class FormRegister extends Component {
    state = {
        values: {
            userName: '',
            password: '',
            email: '',
            fullName: '',
            phone: '',
            user: '',
        },
        errors: {
            userName: '',
            password: '',
            email: '',
            fullName: '',
            phone: '',
            user: '',
        },
        checkRegex: {
            userName: '',
            password: '',
            email: '',
            fullName: '',
            phone: '',
            user: '',
        },

    }
    getValue = (event) => {
        let { name, value } = event.target;
        let newCheckRegex = { ...this.state.checkRegex };
        let newErrors = { ...this.state.errors };
        if (value.trim() === '') {
            newErrors[name] = `${name} is required!`;
            newCheckRegex[name] = '';
        } else {
            switch (name) {
                case "userName": {
                    let regex = /^[a-zA-Z0-9]+$/;
                    if (!regex.test(value)) {
                        newErrors[name] = <ul>
                            <li>{name} can only container:</li>
                            <li>Lowercase Letters</li>
                            <li>Uppercase Letters</li>
                            <li>Number: 0-9</li>
                        </ul>;
                        newCheckRegex[name] = '';
                    } else {
                        newErrors[name] = ``;
                        newCheckRegex[name] = <i class="fa fa-check-circle"></i>;
                    }
                } break;
                case "email": {
                    let regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (!regex.test(value)) {
                        newErrors[name] = `${name} is invalid!`;
                        newCheckRegex[name] = '';
                    } else {
                        newErrors[name] = ``;
                        newCheckRegex[name] = <i class="fa fa-check-circle"></i>;
                    }
                } break;
                case "phone": {
                    let regex = /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/;
                    if (!regex.test(value)) {
                        newErrors[name] = `${name} is invalid!`;
                        newCheckRegex[name] = '';
                    } else {
                        newErrors[name] = ``;
                        newCheckRegex[name] = <i class="fa fa-check-circle"></i>;
                    }
                } break;
                case 'password': {
                    if (value.length < 6) {
                        newErrors[name] = `Minimum length is 6 characters!`;
                        newCheckRegex[name] = '';
                    } else {
                        newErrors[name] = ``;
                        newCheckRegex[name] = <i class="fa fa-check-circle"></i>;
                    }
                } break;
                default:
                    newErrors[name] = ``;
            }
        }
        let newValues = { ...this.state.values, [name]: value };
        this.setState({
            values: newValues,
            errors: newErrors,
            checkRegex: newCheckRegex,
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        let { register } = this.props;
        let valid = true;
        for (let key in this.state.values) {
            if (this.state.errors[key] !== '' || this.state.values[key] === '') {
                valid = false;
            }
        }
        if (!valid) {
            return;
        }
        let account = { ...this.state.values };
        register(account);

    }
    render() {
        let { values, errors, checkRegex, disabled } = this.state;
        let { changeDisabled, updateAccount } = this.props;
        return (
            <div>
                <H3>Form Register</H3>
                <form onSubmit={this.handleSubmit}>
                    <div className='row'>
                        <div className='col-6 pe-4'>
                            <TextField className='group'>
                                <input value={values.userName} type="text" name='userName' required onChange={this.getValue} onBlur={this.getValue} />
                                <span className="highlight" />
                                <span className="bar" />
                                <label>User Name</label>
                                <span className='text-danger fw-bold text-capitalize'>{errors.userName}</span>
                                <span className='icon-check text-success'>{checkRegex.userName}</span>
                            </TextField>
                            <TextField className='group'>
                                <input value={values.password} type="password" name='password' required onChange={this.getValue} onBlur={this.getValue} />
                                <span className="highlight" />
                                <span className="bar" />
                                <label>Password</label>
                                <span className='text-danger fw-bold text-capitalize'>{errors.password}</span>
                                <span className='icon-check text-success'>{checkRegex.password}</span>
                            </TextField>
                            <TextField className='group'>
                                <input value={values.email} type="text" name='email' required onChange={this.getValue} onBlur={this.getValue} />
                                <span className="highlight" />
                                <span className="bar" />
                                <label>Email</label>
                                <span className='text-danger fw-bold text-capitalize'>{errors.email}</span>
                                <span className='icon-check text-success'>{checkRegex.email}</span>
                            </TextField>
                        </div>
                        <div className='col-6 ps-4'>
                            <TextField className='group'>
                                <input value={values.fullName} type="text" name='fullName' required onChange={this.getValue} onBlur={this.getValue} />
                                <span className="highlight" />
                                <span className="bar" />
                                <label>Full Name</label>
                                <span className='text-danger fw-bold text-capitalize'>{errors.fullName}</span>
                            </TextField>
                            <TextField className='group'>
                                <input value={values.phone} type="text" name='phone' required onChange={this.getValue} onBlur={this.getValue} />
                                <span className="highlight" />
                                <span className="bar" />
                                <label>Phone Number</label>
                                <span className='text-danger fw-bold text-capitalize'>{errors.phone}</span>
                                <span className=' icon-check text-success'>{checkRegex.phone}</span>
                            </TextField>
                            <Select value={values.user} name='user' onChange={this.getValue} onBlur={this.getValue}>
                                <option value='' disabled selected>Please Choose User</option>
                                <option value={1}>Customer</option>
                                <option value={2}>Manager</option>
                            </Select>
                            <p className='text-danger fw-bold text-capitalize'>{errors.user}</p>
                        </div>
                        <div className='col-12 mt-5'>
                            <div className='text-center'>
                                <button disabled={!this.props.disabled} type='submit' className='btn btn-primary me-3' >Register</button>
                                <button disabled={this.props.disabled} type='button' className='btn btn-success' onClick={() => {
                                    this.setState({
                                        values: {
                                            userName: '',
                                            password: '',
                                            email: '',
                                            fullName: '',
                                            phone: '',
                                            user: '',
                                        }
                                    }, () => { changeDisabled(true); updateAccount(values) })
                                }}>Update</button>
                            </div>
                        </div>
                    </div>
                </form>
                <Hr></Hr>
            </div>
        )
    }

    componentDidUpdate(preProps, preState) {
        if (preProps.accountEdit !== this.props.accountEdit) {
            this.setState({
                values: this.props.accountEdit
            })
        }
    }
}
const mapDispatchToProp = (dispatch) => {
    return {
        register: (account) => {
            dispatch(actDegister(account))
        },
        changeDisabled: (val) => {
            dispatch(actChangeDisabled(val))
        },
        updateAccount: (account) => {
            dispatch(actUpdateAccount(account))
        }
    }
}
const mapStateToProp = (state) => {
    return {
        disabled: state.userManagerReducer.disabled,
        accountEdit: state.userManagerReducer.accountEdit,
    }
}

export default connect(mapStateToProp, mapDispatchToProp)(FormRegister)
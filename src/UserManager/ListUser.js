import React, { Component } from 'react'
import { H4 } from '../JSS/components/Heading';
import { Table } from '../JSS/components/Table';
import { connect } from 'react-redux';
import Swal from 'sweetalert2'
import { actChangeDisabled, actDeleteAccount, actEditAccount } from '../redux/actions/userManagerAction';
class ListUser extends Component {
  renderAccount = () => {
    let { deleteAccount, changeDisabled, editAccount } = this.props;
    return this.props.listAccount.map((account, index) => {
      let { userName, password, email, fullName, phone, user } = account;
      return <tr key={index}>
        <th scope="row">{index}</th>
        <td>{userName}</td>
        <td>{fullName}</td>
        <td>{password}</td>
        <td>{email}</td>
        <td>{phone}</td>
        <td>{user == 1 ? "Customer" : "Manager"}</td>
        <td className='text-end'><button className='btn btn-primary' onClick={() => { changeDisabled(false); editAccount(account) }}>Edit</button></td>
        <td className='text-start'><button className='btn btn-danger' onClick={() => { deleteAccount(userName) }}>Delete</button></td>
      </tr>
    })
  }
  render() {
    return (
      <div>
        <div>
          <H4>List User</H4>
          <Table className="table ">
            <thead>
              <tr>
                <th scope="col">STT</th>
                <th scope="col">User Name</th>
                <th scope="col">Full Name</th>
                <th scope="col">Password</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">User Type</th>
              </tr>
            </thead>
            <tbody>
              {this.renderAccount()}
            </tbody>
          </Table>

        </div>
      </div>
    )
  }
}
const mapStateToProp = (state) => {
  return {
    listAccount: state.userManagerReducer.listAccount,
    disabled: state.userManagerReducer.disabled,

  }
}
const mapDispatchToProp = (dispatch) => {
  return {
    deleteAccount: (userName) => {
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(actDeleteAccount(userName));
          Swal.fire(
            'Deleted!',
            'Account has been deleted.',
            'success'
          )
        }
      })
    
    },
    changeDisabled: (val) => {
      dispatch(actChangeDisabled(val))
    },
    editAccount: (account) => {
      dispatch(actEditAccount(account))
    }
  }
}
export default connect(mapStateToProp, mapDispatchToProp)(ListUser)
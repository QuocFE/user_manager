import {combineReducers} from 'redux'
import userManagerReducer from './userManagerReducer'

const rootReducer=combineReducers({
    userManagerReducer
})
export default rootReducer
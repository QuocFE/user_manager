import { darkNight } from '../../JSS/themes/ThemeDarkNight'
import { light } from '../../JSS/themes/ThemeLight'
import Swal from 'sweetalert2'
import { CHANGE_DISABLED, CHANGE_THEME, DELETE, EDIT_ACCOUNT, REGISTER, UPDATE } from "../types/userManagerType"


const stateManager = {
    theme: darkNight,
    listAccount: [],
    accountEdit: '',
    disabled: true,
}

const notification_success = (action) => {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        background: "#46a302",
        color: "#cac5be",
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    Toast.fire({
        icon: 'success',
        html: `<span class="fw-bold">${action} in successfully!</span>`,
    })
}
const checkValueExist = (list, account) => {
    let checkValue = {
        userName: '',
        email: '',
        phone: '',
    };
    let valid = true;
    for (let currAccount of list) {
        for (let key in checkValue) {
            if (currAccount[key] == account[key]) {
                checkValue[key] = `${key} already exists!`;
                valid = false;
            } else {
                checkValue[key] = '';
            }
        }
    }
    let render = () => {
        let content = '';
        Object.keys(checkValue).map((key) => {
            if (checkValue[key] !== '') {
                content += `<p class='text-danger text-start text-capitalize fw-bold'><i class="fa fa-minus-square"></i>  ${checkValue[key]}</p>`
            }
        })
        return content
    }
    if (!valid) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: `
            ${render()}
            `
        })
    }
    return valid;
}
const userManagerReducer = (state = stateManager, action) => {
    switch (action.type) {
        case CHANGE_THEME: {
            let { checked } = action.event.target;
            if (checked) {
                return { ...state, theme: light }
            } else {
                return { ...state, theme: darkNight }
            }
        }
        case REGISTER: {
            let { account } = action;
            let newListAccount = [...state.listAccount];
            if (newListAccount.length === 0) {
                newListAccount.push(account);
                notification_success("Register")
            }
            else {
                let valid = checkValueExist(newListAccount, account);
                if (!valid) {
                    return { ...state }
                }
                newListAccount.push(account);
                notification_success("Register")
            }
            return { ...state, listAccount: newListAccount }
        }
        case DELETE: {
            return { ...state, listAccount: [...state.listAccount.filter(account => account.userName !== action.userName)] }
        }
        case CHANGE_DISABLED: {
            return { ...state, disabled: action.val };
        }
        case EDIT_ACCOUNT: {
            return { ...state, accountEdit: action.account }
        }
        case UPDATE: {
            let { account } = action;
            let index = state.listAccount.findIndex(account => account.userName === state.accountEdit.userName);
            let newList = [...state.listAccount];
            newList[index] = account;
            return { ...state, listAccount: newList }
        }
    }
    return { ...state }
}

export default userManagerReducer
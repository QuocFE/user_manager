import { CHANGE_THEME, DELETE, CHANGE_DISABLED, REGISTER, EDIT_ACCOUNT, UPDATE } from "../types/userManagerType"

export const actChangeTheme=(event)=>{
    return{
        type:CHANGE_THEME,
        event
    }
}

export const actDegister=(account)=>{
    return{
        type:REGISTER,
        account
    }
}

export const actDeleteAccount=(userName)=>{
return{
    type:DELETE,
    userName
}
}

export const actChangeDisabled=(val)=>{
    return{
        type :CHANGE_DISABLED,
        val
    }
}
export const actEditAccount=(account)=>{
    return{
        type:EDIT_ACCOUNT,
        account
    }
}
export const actUpdateAccount=(account)=>{
    return{
        type:UPDATE,
        account
    }
}